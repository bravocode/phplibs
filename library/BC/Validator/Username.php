<?php

namespace BC\Validator;

class Username extends \Zend\Validator\AbstractValidator{

    const USERNAME = "username";

    protected $_messageTemplates = array(
        self::USERNAME => "'%value%' does not meet our standards. Please retry"
    );

    /**
     * Validates Usernames.
     *
     * Only usernames with alphanumeric and underscore(_) characters will pass through.
     * Also the username should start with an alphabet.
     * @param  mixed  $value Username to be validated.
     * @return boolean        Validation result.
     */
    public function isValid($value){
        if (preg_match('/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/', $value)) {
            return true;
        } else {
            return false;
        }
    }
} 

?>