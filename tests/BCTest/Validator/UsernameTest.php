<?php

use BC\Validator\Username;

class UsernameTest extends PHPUnit_Framework_TestCase{

    public function testUsernameIsProperlyValidated(){
        $validator = new Username();
        
        //TRUE Cases
        $username = "shahalpk";
        $this->assertTrue($validator->isValid($username));

        $username = "a_alsjl";
        $this->assertTrue($validator->isValid($username));        


        //FALSE Cases
        $username = "123141";
        $this->assertFalse($validator->isValid($username));

        $username = "_something132";
        $this->assertFalse($validator->isValid($username));

        $username = "ljasfasJ_";
        $this->assertFalse($validator->isValid($username));
    }
}

?>