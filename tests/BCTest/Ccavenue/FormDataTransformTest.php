<?php

use BC\Ccavenue\FormDataTransform;

class FormDataTransformTest extends PHPUnit_Framework_TestCase{

    public function testBasicEncrypt(){
        $testString = "shahal";
        $encString = FormDataTransform::encrypt($testString,"key");
        $decString = FormDataTransform::decrypt($encString,"key");
        $this->assertTrue($testString==trim($decString));
    }
}

?>